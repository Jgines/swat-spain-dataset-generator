# Author: Jose Gines Gimenez Manuel
# email: jggimenez@ucam.edu
# THIS FILE:
# GENERATES A EXCELL FILE WITH THE FORMAT FOR SWAT WGEN (EASILY TO EXPORT TO ACCESS OR SQLITE)


# POR ESTATION
# r5 average dayly max temp
# r6 average dayly min temp
# r7 standar deviation daily max temp
# r8 standar deviation daily min  temp
# r9 average total monthly precipitacion
# r10 standard desviation " "
# r11 skew confficient daily precipitacion in monthly
# r12 probability of a wet day following a dry day
# r13 probability of a dry day following a wet day
# r14 average number of days of precipitacion in month
# r15 extreme 30-min
# r16 average solar radiacion in a monthly
# r17 average daily dew point temperature for each month
# r18 average daily wind speed in month
# Create an engine instance

import psycopg2
import openpyxl
import pandas as pds
import sqlalchemy as alc
import datetime
import numpy as np
import xlsxwriter
import sys


START_DATE = "1979-1-1"
END_DATE = "2013-12-31"

INIT_STAT = sys.argv[1]
END_STAT =  sys.argv[2]


__years = 0

conn = psycopg2.connect(connection parameters)

alchemyEngine   = alc.create_engine('postgresql+psycopg2://connection parameters, pool_recycle=3600);
dbConnection    = alchemyEngine.connect();

def radiacion_solar(estacion, D):
    df  = pds.read_sql("Select * from radiacion_solar where estacion = " + str(estacion)
                        + " and fecha BETWEEN '" + START_DATE  + "' and '" + END_DATE +  "'", dbConnection);
    # pds.set_option('display.expand_frame_repr', False);
    df['fecha'] = pds.to_datetime(df['fecha'], format="%Y/%m/%d")
    df.index = df['fecha']
    df['mes'] = df.index.month

    analisis = df[['mes','radiacion']]
    porMeses=  analisis.groupby('mes')

    D['solarav'] = porMeses.mean()

    return D


def humedad(estacion, D):
    df  = pds.read_sql("Select * from humedad where estacion = " + str(estacion) + " and fecha BETWEEN '" + START_DATE  + "' and '" + END_DATE +  "'", dbConnection)
    # pds.set_option('display.expand_frame_repr', False);
    df['fecha'] = pds.to_datetime(df['fecha'], format="%Y/%m/%d")
    df.index = df['fecha']

    df['mes'] = df.index.month

    analisis = df[['mes','humedad_relativa']]
    porMeses =  analisis.groupby('mes')
    D['dewpt'] = porMeses.mean()

    return D

def viento(estacion, D):
    df  = pds.read_sql("Select * from viento where estacion = " + str(estacion)+ " and fecha BETWEEN '" + START_DATE  + "' and '" + END_DATE +  "'", dbConnection)
    # pds.set_option('display.expand_frame_repr', False);
    df['fecha'] = pds.to_datetime(df['fecha'], format="%Y/%m/%d")
    df.index = df['fecha']
    df['mes'] = df.index.month

    analisis = df[['mes','velocidad']]
    porMeses =  analisis.groupby('mes')
    D['wndav'] = porMeses.mean()

    return D

def temperatura(estacion, D):
    df = pds.read_sql("Select * from temperatura where estacion = " + str(estacion) + " and fecha BETWEEN '" + START_DATE  + "' and '" + END_DATE +  "'", dbConnection);
    # pds.set_option('display.expand_frame_repr', False);
    df['fecha'] = pds.to_datetime(df['fecha'], format="%Y/%m/%d")
    df.index = df['fecha']
    df['mes'] = df.index.month

    analisis = df[['mes','max', 'min']]
    porMeses=  analisis.groupby('mes')
    # df.index = pds.to_datetime(df.fecha)
    D['tmpmx'] = porMeses.mean()['max']
    D['tmpmn'] = porMeses.mean()['min']
    D['tmpstdmx'] = porMeses.std()['max']
    D['tmpstdmn'] = porMeses.std()['min']

    return D
    # min = df['min']
    # D['tmpmn'] = min.resample('M').mean()
    # D['tmpstmn']  = min.resample('M').std()



def precipitacion(estacion):

    global __years
    df  = pds.read_sql("Select * from precipitacion where estacion = " + str(estacion) + " and fecha BETWEEN '" + START_DATE  + "' and '" + END_DATE +  "' Order by fecha asc", dbConnection);
    # pds.set_option('display.expand_frame_repr', False);
    df['fecha'] = pds.to_datetime(df['fecha'], format="%Y/%m/%d")
    df.index = df['fecha']

    df['mes'] = df.index.month

    s = df.iloc[0]
    f = df.iloc[-1]
    __years =   (f['fecha'].year - s['fecha'].year) + 1

    years = __years

    df.loc[(df['acumulado_diario'].shift() == 0) & (df['acumulado_diario']>0), 'wetdry']= 1
    df.loc[(df['acumulado_diario'].shift() > 0) & (df['acumulado_diario']>0), 'wetwet']= 1

    # df['dryWet'] = df['acumulado_diario'].shift().where((df['acumulado_diario'].shift() > 0) & (df['acumulado_diario']==0), '')
    # D['pcpmm']  = df['acumulado_diario'].resample('M').mean()
    analisis = df[['mes','acumulado_diario']]
    porMeses=  analisis.groupby('mes')

    D = porMeses.sum()/years

    D['pcpmm'] = D['acumulado_diario']
    D['pcpstd'] = porMeses.std()
    D['pcpskw'] = porMeses.skew()

    mojados = df[['mes','wetwet']]
    secos = df[['mes','wetdry']]

    D['wetwet'] = mojados.groupby('mes').sum() #.apply(lambda x: (x['wetwet'] != None).count())
    D['wetdry'] = secos.groupby('mes').sum()
    D['wetdays'] = analisis[analisis.acumulado_diario > 0].groupby('mes').count() #.apply(lambda x: (x['wetwet'] != None).count())
    D['drydays'] = analisis[analisis.acumulado_diario == 0].groupby('mes').count()
    # D['drydays'] = df[['mes','drydays']].groupby('mes').apply(lambda x: (x != None).count())
    #
    # D['wetdry'] = df[['mes','wetdry']].groupby('mes').sum()
    # D['wetwet'] = df['wetwet'].resample('M').sum()    # D['a'] = acDia.resample('M').apply(loop)
    D['pr_w1_'] = D['wetdry']/D['drydays']
    D['pr_w2_'] = D['wetwet']/D['wetdays']
    D['pcpd'] = D['wetdays']/years

    RAIN  = pds.read_sql("Select mes, medicion from rain_half_hour r left join relacion e on e.aemet=r.nombre where e.id = " + str(estacion), dbConnection);
    RAIN.index = RAIN['mes']
    D['rainhhrmx'] = RAIN['medicion']
 #   print("ESTACION " + str(estacion))

    D.drop(['acumulado_diario', 'wetdays', 'drydays','wetdry','wetwet'], axis=1, inplace=True)

    return D

__first_call = True
__file = 1

def to_excell(worksheet, D, row):
    # print(D)
    global __first_call
    global __file
    global __years
    letra = 9 #donde empiezan a insertarse los estadisticos
    # columEx = chr(ord('@')+1) #A
    # worksheet.write('A'+srt(filaEx), id)

    id = row[0]
    lat = row[1]
    lon = row[2]
    alt = row[3]

    filaEx = __file
    worksheet.write(0 ,0, 'OBJECTID')
    worksheet.write(0 ,1, 'STATE')
    worksheet.write(0 ,2, 'STATION')
    worksheet.write(0 ,3, 'LSTATION')
    worksheet.write(0 ,4, 'ID')
    worksheet.write(0 ,5, 'WLATITUDE')
    worksheet.write(0 ,6, 'WLONGITUDE')
    worksheet.write(0 ,7, 'WELV')
    worksheet.write(0 ,8, 'RAINYEARS')

    worksheet.write(filaEx, 0, id)
    worksheet.write(filaEx, 1, "NA")
    worksheet.write(filaEx, 2, "Wlat")
    worksheet.write(filaEx, 3, "Wlon")
    worksheet.write(filaEx, 4, id)
    worksheet.write(filaEx, 5, lat)
    worksheet.write(filaEx, 6, lon)
    worksheet.write(filaEx, 7, alt)
    worksheet.write(filaEx, 8, __years)

    # cada mes
    for row in range(1, 13):
        mes = D.loc[row]

        for index, value in mes.items():
            if __first_call:
                worksheet.write(0, letra, (index + str(row)).upper())

            worksheet.write(filaEx, letra, value)
            letra = letra + 1

    __first_call = False
    __file = __file + 1

    print("Copiada a excell estacion " + str(id))


def start(hoja):

    count = 0
    query="SELECT * FROM estacion WHERE id between " + str(INIT_STAT) + " AND " + str(END_STAT) + " ORDER BY id ASC"
    with conn:
        with conn.cursor() as curs:
            curs.execute(query)
            records = curs.fetchall()

            final = pds.DataFrame()
            # final["estacion"] = range(1,len(records))
            # final.index = final["estacion"]
            for row in records:

                print("ESTACION " + str(row[0]))
                id = row[0]
                lat = row[1]
                lon = row[2]
                alt = row[3]

                #debe ser el primero porque genera D index.
                D = precipitacion(row[0])
                D = temperatura(row[0], D)
                D = radiacion_solar(row[0], D)
                D = humedad(row[0], D)
                D = viento(row[0],D)

                to_excell(hoja, D, row)


                count = count + 1


try:
    workbook = xlsxwriter.Workbook('estaciones-' + str(INIT_STAT) + '-' + str(END_STAT) + '.xlsx')
    hoja = workbook.add_worksheet()
    st = datetime.datetime.now()
    start(hoja)
except Exception as e:
    print(str(e))

finally:
    conn.close()
    dbConnection.close();
    workbook.close()
    fin = datetime.datetime.now()
    # total=fin-st
    print("END")
