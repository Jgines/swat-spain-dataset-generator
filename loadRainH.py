# Author: Jose Gines Gimenez Manuel
# email: jggimenez@ucam.edu
# THIS FILE:
# LOADS DATA FROM EXTREME 30 MIN RAIN 

import glob
import logging
import csv
import psycopg2
from datetime import datetime, timedelta

conn = psycopg2.connect(CONNECTION PARAMETERS)


def loadRain():
    query = "INSERT INTO rain_half_hour (nombre, mes, medicion) VALUES "
    with open('../documentacion/RAINHHMX.txt') as file:
        line = file.readline()
        # headers
        line = file.readline()
        while line:
            values = line.split(",")
            mes = 1

            nombre = ''
            for medicion in values:
                if nombre == '':
                    nombre = medicion
                    continue
                else:
                    q = query + "('" + nombre.replace('"', '') + "'," + str(mes) + "," + str(medicion) + ")"
                mes = mes + 1
                with conn:
                    with conn.cursor() as curs:
                        curs.execute(q)



            line = file.readline()

try:
    q = "DELETE FROM rain_half_hour"
    with conn:
        with conn.cursor() as curs:
            curs.execute(q)
    loadRain()
except Exception as e:
    print(str(e))
finally:
    conn.close()
