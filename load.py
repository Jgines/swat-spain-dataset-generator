# Author: Jose Gines Gimenez Manuel
# email: jggimenez@ucam.edu
# THIS FILE:
# LOADS FILES FROM DIFFERNENT SOURCES (AND FORMAT) INTO A DATABASE FOR FUTHER ANALISYS


import glob
import logging
import csv
import psycopg2
from datetime import datetime, timedelta

segundos = 86400

BEGININ = -599574259 # 1-1-1951



conn = psycopg2.connect(CONNCTION PARAMETERS)


def createStation(id, lat, lon, alt):
    query = "INSERT INTO estacion VALUES (" + str(id) + "," + str(lat) + "," + str(lon) + "," + str(alt) + ")"
    print("creando estacion " + str(id))
    with conn:
        with conn.cursor() as curs:
            curs.execute(query)


def insertPCP(id, txt):
    print("READING " + txt)

    query = "INSERT INTO precipitacion (estacion, fecha, acumulado_diario) VALUES "
    with open('../documentacion/AEMET/' + txt + ".txt") as file:
        line = file.readline()
        # values = line.split(".")
        # fecha = int(line)

        fecha = datetime(1951, 1, 1)
        date =fecha.strftime('%Y-%m-%d')

        # date = datetime.fromtimestamp(fecha).strftime('%Y-%m-%d')
        # print(4)
        cnt = 1
        line = file.readline()
        while line:
            q = query + "(" + str(id) + ",'" +  date + "'," + str(line) + ")"
            with conn:
                with conn.cursor() as curs:
                    curs.execute(q)

            # fecha = fecha + segundos
            # date = datetime.fromtimestamp(fecha).strftime('%Y-%m-%d')
            fecha = fecha + timedelta(1)
            date =fecha.strftime('%Y-%m-%d')
            line = file.readline()
            cnt += 1

        print("leido " + txt + " filas " + str(cnt) + " hasta " + date)


def inserTemp(id, txt):
    query = "INSERT INTO temperatura (estacion, fecha, max, min) VALUES "
    with open('../documentacion/SPAIN02/' + txt + ".txt") as file:
        line = file.readline()
        ostia = line.split(",")
        # fecha = ostia[0]
        # fecha = int(line.split(",")[0])

        # fecha = BEGININ
        # date = datetime.fromtimestamp(fecha).strftime('%Y-%m-%d')
        fecha = datetime(1950, 1, 1)
        date =fecha.strftime('%Y-%m-%d')

        cnt = 1
        line = file.readline()
        while line:
            values = line.split(",")

            if len(values)==2:

                q = query + "(" + str(id) + ",'" +  date +  "'," + str(values[0]) + "," + str(values[1]) + ")"

                with conn:
                    with conn.cursor() as curs:
                        curs.execute(q)

            line = file.readline()
            # fecha = fecha + segundos
            # date = datetime.fromtimestamp(fecha).strtime('%Y-%m-%d')
            fecha = fecha + timedelta(1)
            date =fecha.strftime('%Y-%m-%d')

            cnt += 1
    print("leido " + txt + " filas " + str(cnt) + " hasta " + date)

def inserOthers(id, txt):

    insert = ("radiacion_solar, viento, humedad")
    qR = "INSERT INTO radiacion_solar (estacion, fecha, radiacion) VALUES "
    qH = "INSERT INTO humedad (estacion, fecha, humedad_relativa) VALUES "
    qV = "INSERT INTO viento (estacion, fecha, velocidad) VALUES "
    cnt = 0
    with open('../documentacion/CSFR/' + txt + ".csv") as file_csv:

        csv_reader = csv.reader(file_csv, delimiter=',')
        for row in csv_reader:
            if(cnt==0):
                cnt += 1
                continue
            fecha = datetime.strptime(row[0], '%m/%d/%Y')
            fecha = fecha.strftime('%Y-%m-%d')
            qr = qR + "(" + str(id) + ",'" + fecha + "'," + str(row[9]) + ");"
            qh = qH + "(" + str(id) + ",'" + fecha  + "'," + str(row[8]) + ");"
            qv = qV + "(" + str(id) + ",'" + fecha  + "'," + str(row[7]) + ");"
            cnt += 1
            with conn:
                with conn.cursor() as curs:
                    curs.execute(qv + qh + qr)

    print("leido " + txt + " filas " + str(cnt) + " hasta " + fecha)

def start():
    print("Start")
    query = "select * from relacion r left join estaciones e on r.aemet = e.nombre"
    with conn:
            with conn.cursor() as curs:
                curs.execute(query)
                if not curs.description is None:
                    records = curs.fetchall()
                    for row in records:
                        aemet = row[0]
                        csfr = row[1]
                        spain = row[2]

                        id =row[3]
                        lat =row[6]
                        lon=row[7]
                        alt =row[8]

                        createStation(id, lat, lon, alt)
                        insertPCP(id, aemet)
                        inserTemp(id, spain)
                        inserOthers(id, csfr)


print("Starting...")
try:
    print("deleting....")

    st = datetime.now()
    query="DELETE FROM estacion;DELETE FROM precipitacion; DELETE FROM temperatura;DELETE FROM radiacion_solar;DELETE FROM humedad; DELETE FROM viento;"
    with conn:
            with conn.cursor() as curs:
                curs.execute(query)

    print("Deleted....")
    start()
except Exception as e:
    print(str(e))

finally:
    conn.close()
    fin = datetime.now()
    total=fin-st
    print(total)
